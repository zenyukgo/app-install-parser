package main

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"
	"sync"

	"gitlab.com/zenyuk_go/app-install-parser/model"
)

type allComputers map[model.CompID]model.ComputerInstallations

var mux sync.RWMutex

func main() {
	const readChunkLimit = 100000

	if len(os.Args) < 3 {
		println("missing arguments; first argument - path to a CSV file; second - comma separated list of application IDs")
		os.Exit(1)
	}
	filePath := os.Args[1]
	routinesLimit, err := routinesCount(filePath)
	if err != nil {
		println("invalid argument - path to CSV file")
		os.Exit(1)
	}

	appIDsStrings := strings.Split(os.Args[2], ",")
	appIDs := []model.AppID{}
	for _, idStr := range appIDsStrings {
		appID, err := strconv.Atoi(idStr)
		if err != nil {
			panic("application ID is not valid")
		}
		appIDs = append(appIDs, model.AppID(appID))
	}

	allComps := allComputers{}
	var dataChan = make(chan *model.InstallationRaw, 100)
	var wg sync.WaitGroup

	go processRaw(dataChan, &allComps)

	for i := uint64(0); i < routinesLimit; i++ {
		wg.Add(1)
		go readChunk(filePath, uint64(i*readChunkLimit), readChunkLimit, appIDs, dataChan, &wg)
	}

	wg.Wait()
	close(dataChan)
	result := countLicenses(allComps)
	for app, count := range result {
		fmt.Printf("Application %d\t reqires %d licenses\n", app, count)
	}
}

// returns number of routines required to parse file of particular size or error
func routinesCount(filePath string) (uint64, error) {
	fileInfo, err := os.Stat(filePath)
	if err != nil {
		return 0, errors.New("can't get file info")
	}
	// approximately 250 routines per 1 GB of CSV file (in current implementation)
	return uint64(fileInfo.Size() / 1024 / 1024 / 4), nil
}

// readChunk sends parsed, not structured raw data to a dataChan channel
func readChunk(path string, fromLine uint64, limit int, appIDs []model.AppID, dataChan chan<- *model.InstallationRaw, wg *sync.WaitGroup) {
	file, err := os.Open(path)
	defer file.Close()
	if err != nil {
		panic(err.Error())
	}
	skipCount := uint64(0)
	readCount := 0
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		if fromLine == 0 && readCount == 0 {
			fromLine = 1
			// skip header
			continue
		}
		if skipCount < fromLine {
			skipCount++
			continue
		}
		if readCount == limit {
			wg.Done()
			return
		}
		raw, err := parseInstallation(scanner.Text())
		if err != nil {
			// log error & try to count the rest
			println(err.Error())
		}
		if err == nil {
			for _, id := range appIDs {
				if raw.ApplicationID == id {
					dataChan <- &raw
				}
			}
		}
		readCount++
	}
	wg.Done()
}

func countLicenses(allComps allComputers) map[model.AppID]int {
	laptopInstallations := map[model.AppID]int{}
	desktopInstallations := map[model.AppID]int{}
	for _, c := range allComps {
		for appID, installations := range c {
			for _, installation := range installations {
				if installation.Desktop {
					_, ok := desktopInstallations[appID]
					if ok {
						desktopInstallations[appID]++
					} else {
						desktopInstallations[appID] = 1
					}
				} else {
					_, ok := laptopInstallations[appID]
					if ok {
						laptopInstallations[appID]++
					} else {
						laptopInstallations[appID] = 1
					}
				}
			}
		}
	}
	return mergeLaptopsAndDesktops(laptopInstallations, desktopInstallations)
}

func mergeLaptopsAndDesktops(laptops, desktops map[model.AppID]int) map[model.AppID]int {
	result := map[model.AppID]int{}
	for appID, lInstalls := range laptops {
		dInstalls, ok := desktops[appID]
		if ok {
			result[appID] = dInstalls + lInstalls - 1
		} else {
			// two laptops require a single license
			if lInstalls > 1 {
				lInstalls--
			}
			result[appID] = lInstalls
		}
	}
	for appID, dInstalls := range desktops {
		_, ok := laptops[appID]
		if !ok {
			result[appID] = dInstalls
		}
	}
	return result
}

func processRaw(dataChan <-chan *model.InstallationRaw, allComps *allComputers) {
	for {
		select {
		case raw := <-dataChan:
			if raw != nil {
				aggregateInstallation(raw, allComps)
			}
		}
	}
}

func aggregateInstallation(raw *model.InstallationRaw, all *allComputers) {
	mux.RLock()
	appInstallations, compFound := (*all)[raw.ComputerID]
	mux.RUnlock()
	if compFound {
		installations, appfound := appInstallations[raw.ApplicationID]
		if appfound {
			newInstallations := installations[:0]
			for _, installation := range installations {
				if installation.Desktop != raw.Desktop {
					newInstallations = append(newInstallations, model.Installation{
						Desktop: raw.Desktop,
						Source:  raw.Source,
					})
				}
			}
			installations = append(installations, newInstallations...)
		} else {
			i := model.Installation{
				Desktop: raw.Desktop,
				Source:  raw.Source,
			}
			appInstallations[raw.ApplicationID] = []model.Installation{i}
		}
	} else {
		i := model.Installation{
			Desktop: raw.Desktop,
			Source:  raw.Source,
		}
		mux.Lock()
		(*all)[raw.ComputerID] = model.ComputerInstallations{
			raw.ApplicationID: []model.Installation{i},
		}
		mux.Unlock()
	}
}

func parseInstallation(s string) (result model.InstallationRaw, err error) {
	const laptop = "laptop"

	parts := strings.Split(s, ",")
	if len(parts) != 5 {
		err = errors.New("can't parse string - wrong number of elements: " + s)
		return
	}
	for _, part := range parts {
		part = strings.TrimSpace(part)
	}

	cid, err := strconv.Atoi(parts[0])
	if err != nil {
		err = errors.New("can't parse string - wrong Computer ID: " + parts[0])
		return
	}
	result.ComputerID = model.CompID(cid)

	aid, err := strconv.Atoi(parts[2])
	if err != nil {
		err = errors.New("can't parse string - wrong Application ID: " + parts[0])
		return
	}
	result.ApplicationID = model.AppID(aid)
	computerType := strings.ToLower(parts[3])
	if computerType == laptop {
		result.Desktop = false
	} else {
		result.Desktop = true
	}

	source := parts[4]
	result.Source = rune(source[len(source)-1])
	return
}
