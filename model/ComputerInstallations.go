package model

// AppID - application ID
type AppID int

// AppInstallations - installations of a particular application
type AppInstallations []Installation

// ComputerInstallations - all installations on a single computer
type ComputerInstallations map[AppID]AppInstallations
